﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 i = GetBaseInput();
        i = i * Time.deltaTime;

        transform.Translate(i);
    }

    private Vector3 GetBaseInput() {
        Vector3 i_Velocity = new Vector3();

        if (Input.GetKey(KeyCode.W)){
            i_Velocity = new Vector3(0, 0, 1);
        }
        if (Input.GetKey(KeyCode.S)){
            i_Velocity = new Vector3(0, 0, -1);
        }
        if (Input.GetKey(KeyCode.D)){
            i_Velocity = new Vector3(1, 0, 0);
        }
        if (Input.GetKey(KeyCode.A)){
            i_Velocity = new Vector3(-1, 0, 0);
        }
        if (Input.GetKey(KeyCode.E)){
            i_Velocity = new Vector3(0, 1, 0);
        }
        if (Input.GetKey(KeyCode.C)){
            i_Velocity = new Vector3(0, -1, 1);
        }

        return i_Velocity;

    }
}
